// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header with just documentation.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 * @ingroup xrt
 */

#pragma once

/*!
 * @defgroup xrt Monado(XRT)
 */

/*!
 * @defgroup xrt_iface XRT interfaces
 *
 * The main interface shared between the different components of Monado.
 *
 * @ingroup xrt
 */
